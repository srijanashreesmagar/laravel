## Laravel Module

It will quickly generate the scaffolding of folders needed for module.

## Command List

### 1. Create a Module

The command below will quickly generate the basic folders needed for a module.
 
~~~php
    php artisan make:module {module-name}
~~~

For instance, if you need to create a shop module, then you should run a command given below

~~~php
    php artisan make:module Shop
~~~
 


